//Lily Mendoza
//This program prints an ascii character and a short greeting
public class E1_11 {
    public static void main(String[] args) {
        System.out.println("                                 _______________");
        System.out.println("       (  \\                    / HEY THERE!      \\");
        System.out.println("        \\  \\  ,,,,  (  \\      <  HOW'S IT HOPPIN? |");
        System.out.println("         \"  \"      \"\"\\  \\      \\_________________/ ");
        System.out.println("         |   0   0   \"\\  ) ");
        System.out.println("         \"     Y     \" ");
        System.out.println("           '\"'   '\"'   ");
        System.out.println("      __\"              \"__");
        System.out.println("     / \"                \" \\");
        System.out.println("    |  \"               \"   |");
        System.out.println("   (___ |    |___|    | ____)");
        System.out.println("        (____|   |____)");
        System.out.println("");


    }
}
